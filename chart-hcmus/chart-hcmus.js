
// Hàm serialize để tạo tệp SVG và trả về URL của nó
function serialize() {
  const xmlns = "http://www.w3.org/2000/xmlns/";
  const xlinkns = "http://www.w3.org/1999/xlink";
  const svgns = "http://www.w3.org/2000/svg";
  let svg = document.querySelector("svg").cloneNode(true);
  const fragment = window.location.href + "#";
  const walker = document.createTreeWalker(svg, NodeFilter.SHOW_ELEMENT);

  while (walker.nextNode()) {
    for (const attr of walker.currentNode.attributes) {
      if (attr.value.includes(fragment)) {
        attr.value = attr.value.replace(fragment, "#");
      }
    }
  }

  svg.setAttributeNS(xmlns, "xmlns", svgns);
  svg.setAttributeNS(xmlns, "xmlns:xlink", xlinkns);
  const serializer = new window.XMLSerializer();
  const string = serializer.serializeToString(svg);

  const blob = new Blob([string], { type: "image/svg+xml" });
  const url = URL.createObjectURL(blob);
  return url;
}

// Hàm để tạo và tải xuống tệp SVG
function downloadSVG() {
  const svgUrl = serialize();
  const a = document.createElement("a");
  a.href = svgUrl;
  a.download = "chart.svg"; // Tên tệp tải xuống
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
  URL.revokeObjectURL(svgUrl);
}

// Lắng nghe sự kiện click trên nút "Save Chart"
const saveChartButton = document.getElementById("saveChartButton");
if (saveChartButton) {
  saveChartButton.addEventListener("click", downloadSVG);
}

//thêm trục x và y
function drawAxisLabels(svg, xScale, yScale) {
  const labelsX = [0, 10, 20, 30, 40, 50];
  const labelsY = [0, 10, 20, 30, 40, 50];
  const height = 1000
  // Số trên trục x
  svg.selectAll(".x-label")
    .data(labelsX)
    .enter()
    .append("text")
    .attr("class", "x-label")
    .attr("x", (d) => xScale(d))
    .attr("y", height + 20)
    .attr("font-size", "12px")
    .attr("text-anchor", "middle")
    .text((d) => d);

  // Số trên trục y
  svg.selectAll(".y-label")
    .data(labelsY)
    .enter()
    .append("text")
    .attr("class", "y-label")
    .attr("x", -10)
    .attr("y", (d) => yScale(d))
    .attr("font-size", "12px")
    .attr("text-anchor", "end")
    .attr("alignment-baseline", "middle")
    .text((d) => d);
}

function createLegend(svg, familyShapes, familyColors, shapes) {
  // Xác định kích thước và vị trí của chú thích
  const legendX = 0 ; // Bạn có thể thay đổi 20 để đặt chú thích một cách tốt nhất
  const legendY = 20;
  const legendSpacing = 30;

  // Tạo SVG mới cho chú thích
  const legendSvg = d3
    .select("body")
    .append("svg")
    .attr("width", 200)
    .attr("height", 900)
    .style("margin-left", "50px"); // Khoảng cách giữa SVG biểu đồ và SVG chú thích

  // Tạo một nhóm SVG để chứa tất cả các thành phần của chú thích
  const legend =  legendSvg.append('g')
  .attr('transform', `translate(${legendX},${legendY})`);
  
  // Tạo một nhóm con cho mỗi họ
  const legendItems = legend.selectAll('.legend-item')
    .data(Object.entries(familyShapes))
    .enter().append('g')
      .attr('class', 'legend-item')
      .attr('transform', (d, i) => `translate(0,${i * legendSpacing})`);
  
 
  // Tạo một nhóm con cho mỗi họ
  legendItems.append('text')
    .attr('class', 'fa-icon')
    .attr('x', 0)
    .attr('y', 0)
    .attr('font-family', 'FontAwesome')
    .attr('font-size', '17px')
    .attr('fill', (d) => familyColors[d[0]])
    .text((d) => shapes[d[1]]);
  
  // Thêm nhãn tương ứng với mỗi họ
  legendItems.append('text')
    .attr('x', 25) // Thay đổi giá trị này để đặt nhãn đúng vị trí
    .attr('y', 5)  // Điều chỉnh vị trí nhãn theo chiều dọc
    .text((d) => d[0])
    .attr('font-size', '12px');
}
function getRandomColor() {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function Chart() {
  const largeGridSize = 50;
  const width = 1000;
  const height = 1000;

  // Kích thước của một ô vuông nhỏ
  const smallSquareSize = width / 5;

  const svg = d3
    .select("body")
    .append("svg")
    .attr("width", width)
    .attr("height", height);

  // Create scales
  const xScale = d3
    .scaleLinear()
    .domain([0, largeGridSize])
    .range([0, width]);
  const yScale = d3
    .scaleLinear()
    .domain([0, largeGridSize])
    .range([height, 0]);

  // Add the large square
  svg
    .append("rect")
    .attr("x", 0)
    .attr("y", 0)
    .attr("width", width) 
    .attr("height", height)
    .style("fill", "none")
    .style("stroke", "black");
  // Thêm các đường dọc và ngang để tạo ra các ô vuông nhỏ
  for (let i = 1; i < 5; i++) {
    svg
      .append("line")
      .attr("x1", i * smallSquareSize)
      .attr("y1", 0)
      .attr("x2", i * smallSquareSize)
      .attr("y2", height)
      .style("stroke", "grey");

    svg
      .append("line")
      .attr("x1", 0)
      .attr("y1", i * smallSquareSize)
      .attr("x2", width)
      .attr("y2", i * smallSquareSize)
      .style("stroke", "grey");
  }

  // Colors and shapes based on family
  const familyColors = {};
  const familyShapes = {};
  const shapes = {
    "fa-star": "\uf005",
    "fa-heart": "\uf004",
    "fa-cloud":"\uf0c2",
    "fa-comment":"\uf075",
    "fa-circle-half-stroke":"\uf042",
    "fa-folder":"\uf07b",
    "fa-location-pin":"\uf041",
    "fa-calendar":"\uf133",
    "fa-circle":"\uf111",
    "fa-play":"\uf04b",
    "fa-square":"\uf0c8",
    "fa-triangle":"\uf2ec",
    "fa-diamond":"\uf219",
    "fa-certificate":"\uf0a3",
    "fa-spa" : "\uf5bb",
    "fa-sailboat":"\ue445",
    "fa-ticket-simple":"\uf3ff",
    "fa-heart-crack":"\uf7a9",
    "fa-clover":"\ue139",
    "fa-burst":"\ue4dc",
    "fa-cubes-stacked":"\ue4e6",
    "fa-bomb":"\uf1e2",
    "fa-discord":"\uf392",
    "fa-camera-retro":"\uf083",
    "fa-truck-fast":"\uf48b",
    "fa-sghield-halved":"\uf3ed",
    "fa-figma":"\uf799",
    "fa-bell":"\uf0f3",
    "fa-cart-shopping":"\uf07a",
    "fa-clipboard":"\uf328",
    "fa-filter":"\uf0b0",
    "fa-ghost": "\uf6e2",
    "fa-apple" : "\uf179",
    "fa-mug-hot" : "\uf7b6",
    "fa-umbrella" : "\uf0e9",
    "fa-gift" : "\uf06b",
    "fa-film" : "\uf008",
    "fa-list" : "\uf03a",
    "fa-gear" : "\uf013",
    "fa-algolia" : "\uf36c",
    "fa-circle-up" : "\uf35b",

  };
  let shapeKeys = Object.keys(shapes);

  const usedColors = [];
  const usedShapes = [];

  data.forEach((d, i) => {
    if (!familyColors[d.family]) {
      // Get a random color
      let uniqueColorFound = false;
      while (!uniqueColorFound) {
        const potentialColor = getRandomColor();
        
        // Check if this color has been used
        if (!usedColors.includes(potentialColor)) {
          familyColors[d.family] = potentialColor;
          usedColors.push(potentialColor); // add color to used list
          uniqueColorFound = true;
        }
      }
  
      // Get a unique shape
      let uniqueShapeFound = false;
      while (!uniqueShapeFound) {
        const potentialShape = shapeKeys[Math.floor(Math.random() * shapeKeys.length)];
  
        // Check if this shape has been used
        if (!usedShapes.includes(potentialShape)) {
          familyShapes[d.family] = potentialShape;
          usedShapes.push(potentialShape); // add shape to used list
          uniqueShapeFound = true;
        }
      }
    }
  });

  // Tooltip for hovering
  const tooltip = d3
    .select("body")
    .append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

  // Add the tree symbols
  svg
  .selectAll(".fa-icon")
  .data(data)
  .enter()
  .append("text")
  .attr("class", "fa-icon")
  .attr("x", (d) => xScale(d.x))
  .attr("y", (d) => yScale(d.y))
  .attr("font-family", "FontAwesome")
  .attr("font-size", "17px")
  .attr("fill", (d) => familyColors[d.family])
  .text((d) => shapes[familyShapes[d.family]])
  .on("mouseover", function (event, d) {
    tooltip.transition().duration(200).style("opacity", 1);
    tooltip
      .html(`Name: ${d.name}<br/>Diameter: ${d.diameter}cm`)
      .style("left", event.pageX + 5 + "px")
      .style("top", event.pageY - 28 + "px");
  })
  .on("mouseout", function () {
    tooltip.transition().duration(500).style("opacity", 0);
  });

  // Thêm số trên trục x và y dọc theo hình vuông lớn
  drawAxisLabels(svg, xScale, yScale);
  
  const xAxis = d3.axisBottom(xScale);
  const yAxis = d3.axisLeft(yScale);

  svg.append("g").attr("transform", `translate(0,${height})`).call(xAxis);
  svg.append("g").call(yAxis);
  createLegend(svg, familyShapes, familyColors, shapes)
}
Chart();
